<?php
namespace Webtek\Libs\Logger\Transfer\Dummy;

use Webtek\Libs\Logger\Common\Transferable;
use Webtek\Libs\Logger\Transfer\BaseClient;

/**
 * Class Client
 * @package Webtek\Libs\Logger\Transfer\Dummy
 */
class Client extends BaseClient
{

    /**
     * @var int
     */
    protected $mode = self::MODE_SEND_ONE;

    /**
     * @var array
     */
    protected $buffer = array();

    /**
     * @param mixed $flag
     */
    public function setSendFlag($flag)
    {
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        $this->mode = (int) $mode;
    }

    /**
     * @param Transferable $message
     */
    public function send(Transferable $message)
    {
    }

    /**
     * @return void
     */
    public function sendBufferedMessages()
    {
        if (count($this->buffer) > 0) {
            $this->buffer = array();
        }
    }
}

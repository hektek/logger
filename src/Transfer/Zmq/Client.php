<?php

namespace Webtek\Libs\Logger\Transfer\Zmq;

use Webtek\Libs\Logger\Common\Transferable;
use Webtek\Libs\Logger\Exceptions\TransferException;
use Webtek\Libs\Logger\Transfer\BaseClient;

class Client extends BaseClient
{
    const MAX_BUFFER_SIZE = 30;

    protected $sendFlag = \ZMQ::MODE_DONTWAIT;

    protected $address, $port;
    protected $type = \ZMQ::SOCKET_PUSH;
    protected $mode = self::MODE_SEND_ONE;

    /**
     * @var \ZMQSocket
     */
    protected $client = null;
    protected $buffer = array();

    protected $socketOptions = array(
        \ZMQ::SOCKOPT_LINGER => 10000,
        \ZMQ::SOCKOPT_SNDHWM => 10000,
    );

    /**
     * @param string $address
     * @param int $port
     * @throws TransferException
     */
    public function __construct($address, $port)
    {
        if (!$address || !$port) {
            throw new TransferException('Address and port must be provided');
        }

        $this->address = $address;
        $this->port = (int) $port;
    }

    /**
     * @param mixed $flag
     */
    public function setSendFlag($flag)
    {
        $this->sendFlag = $flag;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        $this->mode = (int) $mode;
    }

    /**
     * @param Transferable $message
     */
    public function send(Transferable $message)
    {
        $transferData = $message->getTransferData();

        if ($this->mode === self::MODE_SEND_BUFFER) {
            $this->bufferMessage($transferData);
            return;
        }

        $this->doSend($transferData);
    }

    /**
     * @return void
     */
    public function sendBufferedMessages()
    {
        if (count($this->buffer) > 0) {
            $this->doSend(sprintf('[%s]', implode(',', $this->buffer)));
            $this->buffer = array();
        }
    }

    /**
     * @param $msg
     * @return bool|void
     */
    protected function bufferMessage($msg)
    {
        if (!$msg) {
            return false;
        }

        $this->buffer[] = $msg;

        if (count($this->buffer) >= self::MAX_BUFFER_SIZE) {
            $this->sendBufferedMessages();
        }
    }

    /**
     * @param $json
     * @return bool|void
     */
    protected function doSend($json)
    {
        if (!$json) {
            return false;
        }

        $this->connect();
        $this->client->send($json, $this->sendFlag);
    }

    /**
     * @return bool|void
     */
    protected function connect()
    {
        if ($this->client) {
            return true;
        }

        $context = new \ZMQContext();
        $this->client = new \ZMQSocket($context, $this->type);

        foreach ($this->socketOptions as $key => $value) {
            $this->client->setSockOpt($key, $value);
        }

        $this->client->connect("tcp://{$this->address}:{$this->port}");
    }
}

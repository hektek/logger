<?php
namespace Webtek\Libs\Logger\Transfer;

use Webtek\Libs\Logger\Common\Transferable;

interface ClientInterface
{

    const MODE_SEND_ONE = 1;
    const MODE_SEND_BUFFER = 2;

    /**
     * @param mixed $flag
     */
    public function setSendFlag($flag);

    /**
     * @param mixed $type
     */
    public function setType($type);

    /**
     * @param int $mode
     */
    public function setMode($mode);

    /**
     * @param Transferable $message
     */
    public function send(Transferable $message);

    /**
     * @return void
     */
    public function sendBufferedMessages();
}

<?php

namespace Webtek\Libs\Logger\Transfer\Tcp;

use Webtek\Libs\Logger\Common\Transferable;
use Webtek\Libs\Logger\Exceptions\TransferException;
use Webtek\Libs\Logger\Transfer\BaseClient;

class Client extends BaseClient
{

    /**
     * @var string
     */
    private $address;

    /**
     * @var int
     */
    private $port;

    /**
     * @var resource
     */
    private $socket = null;

    /**
     * @var boolean
     */
    private $connected = false;

    /**
     * @param string $address
     * @param int $port
     * @throws TransferException
     */
    public function __construct($address, $port)
    {
        if (!$address || !$port) {
            throw new TransferException('Address and port must be provided');
        }

        $this->address = $address;
        $this->port = (int)$port;
        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if ($this->socket === false) {
            throw new TransferException("socket_create() failed: " . $this->getLastSocketError());
        }
    }


    /**
     * @param mixed $flag
     */
    public function setSendFlag($flag)
    {
        // TODO: Implement setSendFlag() method.
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        // TODO: Implement setType() method.
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        // TODO: Implement setMode() method.
    }

    /**
     * @param Transferable $message
     * @throws TransferException
     */
    public function send(Transferable $message)
    {
        $data = $message->getTransferData();

        if (!$this->connected && $this->connect()) {
            $this->connected = true;
        }

        if (!socket_write($this->socket, $data, strlen($data))) {
            throw new TransferException("socket_write() failed: " . $this->getLastSocketError());
        }
    }

    /**
     * @return void
     */
    public function sendBufferedMessages()
    {
        // TODO: Implement sendBufferedMessages() method.
    }

    /**
     * @return bool
     * @throws TransferException
     */
    private function connect()
    {
        if (!socket_connect($this->socket, $this->address, $this->port)) {
            throw new TransferException("socket_connect() failed: " . $this->getLastSocketError());
        }

        return true;
    }

    /**
     * @return string
     */
    private function getLastSocketError()
    {
        return socket_strerror(socket_last_error($this->socket));
    }
}

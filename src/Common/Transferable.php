<?php

namespace Webtek\Libs\Logger\Common;

interface Transferable
{

	/**
	 * @return string
	 */
	public function getTransferData();
}
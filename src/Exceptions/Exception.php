<?php

namespace Webtek\Libs\Logger\Exceptions;

class Exception extends \Exception
{

    public function __construct($message, $code = 0, \Exception $previous = null)
    {
        parent::__construct('BS Format Exception: ' . $message, $code, $previous);
    }
}
<?php

namespace Webtek\Libs\Logger\Message\App;

use Webtek\Libs\Logger\Common;
use Webtek\Libs\Logger\Message\App\Adapter\TransferableLog;
use Webtek\Libs\Logger\Message\Message;

/**
 * AppLog service message implementation
 * This service is used for most of logging, actions, events, etc.
 *
 * Supporting JsonString format
 * @author derki
 */
class Log extends Message
{

    /**
     * Insert only, on duplicate, error, this one is default
     */
    const METHOD_INSERT = 'INSERT';

    /**
     * Update on duplicate, otherwise insert
     */
    const METHOD_UPDATE = 'UPDATE';

    /**
     * Aggregated increment
     */
    const METHOD_INCREMENT = 'INCREMENT';

    /**
     * Delete, all fields will be used as WHERE clause with this method
     */
    const METHOD_DELETE = "DELETE";

    /**
     * creates app log message instance with all data
     * @param string    $table  table in storage in which data will be stored
     * @param array     $fields array of fields to be inserted in specified table
     * @param string    $method optional, default is insert method
     */
    public function __construct($table, array $fields, $method = null)
    {
        $this->set('table', $table);
        $this->set('fields', $fields);

        if ($method != null) {
            $this->set('method', $method);
        }
    }

    /**
     * @return string
     */
    public function getTransferData()
    {
        return (new TransferableLog($this))->getTransferData();
    }
}

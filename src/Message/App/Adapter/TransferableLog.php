<?php

namespace Webtek\Libs\Logger\Message\App\Adapter;

use Webtek\Libs\Logger\Common;
use Webtek\Libs\Logger\Message\App\Log;

/**
 * Transferable decorator for App's log
 *
 * @author lkozmon
 */
class TransferableLog implements Common\Transferable
{

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Log $log
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * @return string
     */
    public function getTransferData()
    {
        return json_encode($this->log->getMessageData(), JSON_PRESERVE_ZERO_FRACTION);
    }
}

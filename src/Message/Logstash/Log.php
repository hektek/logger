<?php

namespace Webtek\Libs\Logger\Message\Logstash;

use Webtek\Libs\Logger\Message\Logstash\Adapter\TransferableLog;
use Webtek\Libs\Logger\Message\Message;

/**
 * A little experiment for now
 * @see  http://logstash.net/
 */
class Log extends Message
{

    /**
     * @param array $log
     */
    public function __construct(array $log)
    {
        foreach ($log as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * @return string
     */
    public function getTransferData()
    {
        return (new TransferableLog($this))->getTransferData();
    }
}

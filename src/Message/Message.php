<?php

namespace Webtek\Libs\Logger\Message;

use Webtek\Libs\Logger\Common;

/**
 * Template class for message
 */
abstract class Message implements Common\Transferable
{

    /**
     * @var array Message data
     */
    protected $data = [];

    /**
     * @param $key mixed
     * @param $value mixed
     * @return self
     */
    public function set($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key)
    {
        if (!isset($this->data[$key])) {
            return null;
        }

        return $this->data[$key];
    }

    /**
     * @return array Message data
     */
    public function getMessageData()
    {
        return $this->data;
    }
}

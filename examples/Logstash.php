<?php

require '../vendor/autoload.php';

(new \Webtek\Libs\Logger\Transfer\Zmq\Client('192.168.99.100', 10000))->send(
    new \Webtek\Libs\Logger\Message\Logstash\Log([
        123,
        "Description of error", // Your application error description
        123456789, // The player ID
        array("myCustomValue"=>"some data", "count"=>2, "isPlayerActive"=>true)
    ])
);

